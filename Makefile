# Copyright 2015 The Metalogic Software Authors. All rights reserved.
# Use of this source code is governed by an MIT
# license that can be found in the LICENSE file.

all: clean test monit
	go fmt
	make todo

monit: clean
	CGO_ENABLED=0 go build -a -installsuffix cgo -o monit

test: 
	go test -i bitbucket.org/_metalogic_/monit
	go test -v bitbucket.org/_metalogic_/monit
	go test -i bitbucket.org/_metalogic_/monit/service
	go test -v bitbucket.org/_metalogic_/monit/service

lint: 
	@go vet
	@golint .

install: all
	go install

docker: all
	@cp monit docker/build/files/root/monit
	@cp monitor.conf docker/build/files/root/etc/monitor.conf
	@cp -r html inc docker/build/files/root
	sudo docker build -t metalogic/monit docker/build

run: docker
	sudo docker stop monit
	sudo docker rm monit
	sudo docker run -d --name monit -p 8080:8080 metalogic/monit /monit

todo:
	@grep -n ^[[:space:]]*_[[:space:]]*=[[:space:]][[:alnum:]] *.go || true
	@grep -n TODO *.go || true
	@grep -n FIXME *.go || true
	@grep -n BUG *.go || true

clean:
	go clean
	rm -f monit *~
	rm -rf docker/build/files/root/monit docker/build/files/root/etc/monitor.conf docker/build/files/root/html docker/build/files/root/inc
